source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2'
# Use sqlite3 as the database for Active Record
gem 'sqlite3', '~> 1.3', '>= 1.3.13'
# Use Puma as the app server
gem 'puma', '~> 3.11', '>= 3.11.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0', '>= 5.0.7'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '~> 4.1', '>= 4.1.9'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2', '>= 4.2.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5.1', '>= 5.1.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0', '>= 4.0.1'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1', '>= 3.1.11'

# Use ActiveStorage variant
gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '~> 1.3', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '~> 3.1', '>= 3.1.5'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '~> 3.6'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 2.0', '>= 2.0.2'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Trailblazer
gem 'cells-erb', '~> 0.1.0'
gem 'cells-rails', '~> 0.0.9'
gem 'trailblazer-cells', '~> 0.0.3'
gem 'trailblazer-rails', '~> 2.1', '>= 2.1.1'

# Sidekiq
# gem 'sidekiq', '~> 5.1', '>= 5.1.3'
# gem 'sidekiq-cron', '~> 0.6.3'
# gem 'sidekiq-history', '~> 0.0.9'
# gem 'apartment-sidekiq', '~> 1.2'

gem 'devise', '~> 4.4', '>= 4.4.3'
gem 'pundit', '~> 1.1'

gem 'active_link_to', '~> 1.0', '>= 1.0.5'
gem 'active_model_serializers', '~> 0.10.7'
gem 'acts_as_votable', '~> 0.11.1'
gem 'annotate', '~> 2.7', '>= 2.7.2'
# gem 'apartment', '~> 2.2'
gem 'awesome_print', '~> 1.8'
gem 'bootstrap', '~> 4.0'
gem 'cocoon', '~> 1.2', '>= 1.2.11'
gem 'config', '~> 1.7'
gem 'data_migrate', '~> 3.5'
# gem 'deface', '~> 1.3'
# gem 'faraday', '~> 0.14.0'
gem 'figaro', '~> 1.1', '>= 1.1.1'
gem 'file_validators', '~> 2.2.0.beta1'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.4'
gem 'goldiloader', '~> 2.1'
gem 'impressionist', '~> 1.6', '>= 1.6.1'
gem 'jquery-rails', '~> 4.3', '>= 4.3.1'
# gem 'marcel', '~> 0.3.2'
# gem 'metamagic', '~> 3.1', '>= 3.1.7'
# gem 'money-rails', '~> 1.11'
# gem 'geocoder', '~> 1.4', '>= 1.4.7'
gem 'multi_json', '~> 1.13', '>= 1.13.1'
gem 'oj', '~> 3.5', '>= 3.5.1'
gem 'paranoia', '~> 2.4', '>= 2.4.1'
# gem 'premailer-rails', '~> 1.10', '>= 1.10.2'
# gem 'ransack', '~> 1.8', '>= 1.8.8'
gem 'rails_email_validator', '~> 0.1.4'
# gem 'rubyzip', '~> 1.2', '>= 1.2.1'
gem 'simple_form', '~> 4.0'
gem 'wannabe_bool', '~> 0.7.1'
gem 'will_paginate', '~> 3.1', '>= 3.1.6'
gem 'enumerate_it', '~> 1.7'
# gem 'email_reply_trimmer', '~> 0.1.11'
# gem 'trix', '~> 0.11.1'

group :development do
  gem 'binding_of_caller', '~> 0.8.0'
  gem 'bullet', '~> 5.7', '>= 5.7.5'

  gem 'hirb', '~> 0.7.3' # hirb makes pry output even more awesome
  gem 'pry-byebug', '~> 3.6' # kickass debugging
  gem 'pry-doc', '~> 0.13.4' # read ruby docs in console
  gem 'pry-rails', '~> 0.3.6' # pry is awsome
  gem 'pry-stack_explorer', '~> 0.4.9.2' # step through stack

  gem 'letter_opener', '~> 1.6'
end
